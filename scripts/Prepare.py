# import pandas as pd
# from tqdm import tqdm
from difflib import SequenceMatcher
# import re
# import pickle
import numpy as np
import pandas as pd
import glob     
import prep_opt_1
import prep_opt_2


def getInput():
    op1 = "1.Load files from manual_pkl"
    op2 = "2.Load files from augmented_pkl"
    num = (input(f"Enter option 1 or 2: \n{op1}\n{op2} \n"))
    if num.strip().isdigit():
        num = int(num)
        if (num in [1, 2]):
            return num
        else:
            return None

    else:
        return None

while (True):
    option = getInput()
    if (option):
        print("#valid input")
        break
    else:
        print('#invalid input ')


if option == 1:
    prep_opt_1.option_1()

if option == 2:
    prep_opt_2.option_2()
